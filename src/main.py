import os
import struct


"""
([T] O B E O R N O T T O B E), lookback buffer empty.
(T [O] B E O R N O T T O B E), lookback buffer does not contain sequence 'O'.
(T O [B] E O R N O T T O B E), lookback buffer does not contain sequence 'B'.
(T O B [E] O R N O T T O B E), lookback buffer does not contain sequence 'E'.
(T O B E [O] R N O T T O B E), lookback buffer does contain sequence 'O'.
(T O B E [O R] N O T T O B E), lookback buffer does not contain sequence 'O, R'. Point to 'O', put 'R'
(T O B E O R [N] O T T O B E), lookback buffer does not contain sequence 'N'.
(T O B E O R N [O] T T O B E), lookback buffer does contain sequence 'O'.
(T O B E O R N [O T] T O B E), lookback buffer does not contain sequence 'O, T'. Point to 'O', put 'T'
(T O B E O R N O T [T] O B E), lookback buffer does contain sequence 'T'.
(T O B E O R N O T [T O] B E), lookback buffer does contain sequence 'T, O'.
(T O B E O R N O T [T O B] E), lookback buffer does contain sequence 'T, O, B'.
(T O B E O R N O T [T O B E]), lookback buffer does contain sequence 'T, O, B, E'. Point to 'TOBE', end of input
"""


LOOKBACK_SIZE = 4095
COPY_MAX = 15


def main():
    input = None
    use_file = False
    if use_file:
        path = os.path.dirname(os.path.abspath(__file__))
        with open(f"{path}/alice.txt", "r") as f:
            input = f.read()
    else:
        input = "TOBEORNOTTOBEORTOBE"
    
    lookback = "\0" * LOOKBACK_SIZE
    in_length = len(input)

    prefix = ""
    cur_char = ''
    n_char = 0
    temp = 0

    jmp_back = 0
    encoded = bytearray()
    decoded = None

    offset = 0
    while offset < in_length:
        cur_char = input[offset]
        prefix = ""
        n_char = 1
        jmp_back = 0
        temp = 0

        while offset + n_char < in_length:
            temp = find_prefix(lookback, prefix + cur_char)
            if temp != 0 and n_char <= COPY_MAX:
                prefix += cur_char
                cur_char = input[offset + n_char]
                n_char += 1
                jmp_back = temp
            else:
                lookback = lookback_add(lookback, prefix+cur_char)
                break
        
        offset += n_char
        encoded += encode_lz(jmp_back, n_char - 1, cur_char)
    
    if use_file:
        path = os.path.dirname(os.path.abspath(__file__))
        
        if encoded != None:
            with open(f"{path}/alice.txt.lz77", "wb") as f:
                f.write(encoded)
        
            encoded = None
            with open(f"{path}/alice.txt.lz77", "rb") as f:
                encoded = f.read()
            
            decoded = decode_lz(encoded)
            with open(f"{path}/alice_out.txt", "w") as f:
                f.write(decoded)
    else:
        decoded = decode_lz(encoded)
        print(decoded)


def encode_lz(jmp_back, cpy_len, char):
    sequence = bytearray()
    ctrl_char = jmp_back << 4
    ctrl_char = ctrl_char | cpy_len
    sequence += struct.pack('>H', ctrl_char)
    sequence += char.encode('utf-8')
    return sequence


def decode_lz(data):
    decoded = ""
    ctrl_char = 0
    jmp_back = 0
    cpy_len = 0
    offset = 0
    dec_offset = 0
    while offset < len(data):
        ctrl_char = (data[offset] << 8) + data[offset+1]
        jmp_back = ctrl_char >> 4
        cpy_len = ctrl_char & 0x000F
        
        for _ in range(cpy_len):
            decoded += decoded[dec_offset - jmp_back]
            dec_offset += 1
        
        if offset < len(data):
            decoded += chr(data[offset+2])
            dec_offset += 1
            offset += 1
        
        offset += 2
    
    return decoded


def lookback_add(lookback, add):
    for i in range(len(add)):
        lookback = lookback[ 1 : LOOKBACK_SIZE ] + add[i]
    return lookback


def find_prefix(lookback, prefix):
    relative_jmp = lookback.find(prefix, 0, LOOKBACK_SIZE)
    if relative_jmp >= 0:
        return LOOKBACK_SIZE - relative_jmp
    return 0


main()